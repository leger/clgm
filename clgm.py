import numpy as np
import scipy as sp
import scipy.optimize as spo
import scipy.special as sps
import scipy.cluster.vq as spcv

psi = sps.digamma
psi1 = lambda x: sps.polygamma(1,x)

multbetaln = lambda x: np.sum(sps.gammaln(x))-sps.gammaln(np.sum(x))

class ModelBase:
    def __init__(self,Q,X,Y):
        n,n=X.shape
        self.mplf = np.zeros((Q,Q,n,n))
        self.Q = Q
        self.X = X
        self.Y = Y
        self.mplf_precomputed = False

    def precompute(self):
        if self.mplf_precomputed:
            return None
        for q in range(self.Q):
            for l in range(self.Q):
                self.mplf[q,l,:,:] = self.lf(q,l)
        self.mplf_precomputed = True


class ModelGaussian(ModelBase):

    def __init__(self,Q,X,Y):
        ModelBase.__init__(self,Q,X,Y)
        self.mu = np.zeros((Q,Q))
        self.sigma2 = 1
        self.n_params = Q*Q+1
    
    def lf(self,q,l):
        return -.5*np.log(2*np.pi*self.sigma2)-.5/self.sigma2*(self.X-self.mu[q,l])**2

    def dlf(self,q,l):
        d = list()
        for qi in range(self.Q):
            for li in range(self.Q):
                if qi == q and li == l:
                    d.append(1./self.sigma2*(self.X-self.mu[q,l]))
                else:
                    d.append(0)
        d.append(-.5/self.sigma2 +.5/(self.sigma2**2)*(self.X-self.mu[q,l])**2)
        return d

    def get_params(self):
        Q,Q=self.mu.shape
        return np.concatenate((self.mu.reshape(Q*Q),[self.sigma2]))

    def set_params(self,p):
        self.mu = p[0:(self.Q*self.Q)].reshape((self.Q,self.Q))
        self.sigma2 = p[-1]
        self.precompute()

    def get_constraints(self):
        consts = [(None,None)]*(self.n_params-1)
        consts.append((1e-6,None))
        return consts

    def transf(self):
        return self.X
    
    def generate(self,q,l,i,j):
        return self.mu[q,l] + np.sqrt(self.sigma2)*np.random.normal()

class ModelGaussianDiag1(ModelGaussian):
    def __init__(self,Q,X,Y):
        ModelGaussian.__init__(self,Q,X,Y)
        for i in range(self.Q):
            self.mu[i,i] = 1.

    def get_constraints(self):
        consts = [(None,None)]*(self.n_params-1)
        for i in range(self.Q):
            consts[i*self.Q+i] = (1-1e-6,1+1e-6)
        consts.append((1e-6,None))

        return consts

class ModelGaussianCovariates(ModelBase):
    
    def __init__(self,Q,X,Y):
        ModelBase.__init__(self,Q,X,Y)
        self.mu = np.zeros((Q,Q))
        self.sigma2 = 1
        n,n,C = Y.shape
        self.beta = np.zeros(C)
        self.n_params = Q*Q+1+C

    def lf(self,q,l):
        return -.5*np.log(2*np.pi*self.sigma2)-.5/self.sigma2*(self.X-np.array(self.Y*np.matrix(self.beta).T)-self.mu[q,l])**2

    def dlf(self,q,l):
        betaY = np.array(self.Y*np.matrix(self.beta).T)
        xmbmmqlij=1./self.sigma2*(self.X-betaY-self.mu[q,l])
        d = list()
        for qi in range(self.Q):
            for li in range(self.Q):
                if qi == q and li == l:
                    d.append(xmbmmqlij)
                else:
                    d.append(0)
        d.append(-.5/self.sigma2 +.5/(self.sigma2**2)*(self.X-betaY-self.mu[q,l])**2)
        for k in range(len(self.beta)):
            d.append(xmbmmqlij*self.Y[:,:,k])
        return d
    
    def get_params(self):
        Q,Q=self.mu.shape
        return np.concatenate((self.mu.reshape(Q*Q),[self.sigma2],self.beta))

    def set_params(self,p):
        self.mu = p[0:(self.Q*self.Q)].reshape((self.Q,self.Q))
        self.sigma2 = p[self.Q*self.Q]
        self.beta = p[(self.Q*self.Q+1):]
        self.precompute()

    def get_constraints(self):
        consts = [(None,None)]*(self.Q*self.Q)
        consts.append((1e-6,None))
        consts.extend([(None,None)]*len(self.beta))
        return consts

    def transf(self):
        return self.X
    
    def generate(self,q,l,i,j):
        return self.mu[q,l] + (self.Y[i,j,:]*np.matrix(self.beta).T)[0,0] + np.sqrt(self.sigma2)*np.random.normal()

class ModelGaussianCovariatesDiag1(ModelGaussianCovariates):
    def __init__(self,Q,X,Y):
        ModelGaussianCovariates.__init__(self,Q,X,Y)
        for i in range(self.Q):
            self.mu[i,i] = 1.

    def get_constraints(self):
        consts = [(None,None)]*(self.Q*self.Q)
        consts.append((1e-6,None))
        consts.extend([(None,None)]*len(self.beta))
        for i in range(self.Q):
            consts[i*self.Q+i] = (1-1e-6,1+1e-6)
        return consts

class ModelPoisson(ModelBase):

    def __init__(self,Q,X,Y):
        ModelBase.__init__(self,Q,X,Y)
        self.lamb = np.ones((Q,Q))
        self.n_params = Q*Q

    def lf(self,q,l):
        return -self.lamb[q,l] + np.log(self.lamb[q,l])*self.X - sps.gammaln(self.X+1)

    def dlf(self,q,l):
        d = list()
        for qi in range(self.Q):
            for li in range(self.Q):
                if qi == q and li == l:
                    d.append(-1.+self.X/self.lamb[q,l])
                else:
                    d.append(0)
        return d

    def get_params(self):
        return self.lamb.reshape(self.Q*self.Q)

    def set_params(self,p):
        self.lamb = p.reshape((self.Q,self.Q))
        self.precompute()

    def get_constraints(self):
        consts = [(1e-6,None)]*self.n_params
        return consts

    def transf(self):
        return np.log(1+self.X)
    
    def generate(self,q,l,i,j):
        return np.random.poisson(self.lamb[q,l])


class ModelPoissonCovariates(ModelBase):
    
    def __init__(self,Q,X,Y):
        ModelBase.__init__(self,Q,X,Y)
        self.mu = np.zeros((Q,Q))
        n,n,C = Y.shape
        self.beta = np.zeros(C)
        self.n_params = Q*Q+C

    def lf(self,q,l):
        llqlij = np.array( self.mu[q,l] + self.Y*np.matrix(self.beta).T )
        return -np.exp(llqlij) + llqlij*self.X - sps.gammaln(self.X+1)

    def dlf(self,q,l):
        elqlijX = -np.exp(np.array( self.mu[q,l] + self.Y*np.matrix(self.beta).T ))+self.X
        d = list()
        for qi in range(self.Q):
            for li in range(self.Q):
                if qi == q and li == l:
                    d.append(elqlijX)
                else:
                    d.append(0)
        for k in range(len(self.beta)):
            d.append(elqlijX*self.Y[:,:,k])
        return d

    def get_params(self):
        return np.concatenate((self.mu.reshape(self.Q*self.Q),self.beta))

    def set_params(self,p):
        self.mu = p[0:(self.Q*self.Q)].reshape((self.Q,self.Q))
        self.beta = p[(self.Q*self.Q):]
        self.precompute()

    def get_constraints(self):
        return [(None,None)]*self.n_params

    def transf(self):
        return np.log(1+self.X)

    def generate(self,q,l,i,j):
        esp = np.exp(self.mu[q,l] + (self.Y[i,j,:]*np.matrix(self.beta).T)[0,0])
        return np.random.poisson(esp)


def generate_scalar(model, alpha, Y):
    Q,=alpha.shape
    n,n = model.X.shape
    
    # generation of latent layer 1
    W = np.random.dirichlet(alpha,size=n).T

    X = np.zeros((n,n))

    for i in range(n):
        for j in range(n):
            q = list(np.random.multinomial(1,W[:,i])).index(1)
            l = list(np.random.multinomial(1,W[:,j])).index(1)
            X[i,j] = model.generate(q,l,i,j)

    return (W,X)

def psi_gamma_minus_psi_sgamma(gamma):
    Q,n = gamma.shape
    return psi(gamma)-psi(np.sum(gamma,axis=0)).reshape((1,n)).repeat(Q,axis=0)

def H1(gamma):
    Q,n = gamma.shape
    
    return (
            (
                np.sum(map(lambda i: multbetaln(gamma[:,i]), range(n))) 
            ) + (
                -np.sum((gamma-1)*psi_gamma_minus_psi_sgamma(gamma))
                )
           )

def H2(tau):
    return -np.sum(tau*np.log(tau))

def PL1(alpha,gamma):
    Q,n = gamma.shape

    return -n * multbetaln( alpha ) + np.sum((alpha.reshape((Q,1)).repeat(n,axis=1)-1)*psi_gamma_minus_psi_sgamma(gamma))

def PL2(gamma,tau):
    Q,n = gamma.shape

    psmpsg=psi_gamma_minus_psi_sgamma(gamma)

    return np.sum(map(lambda j:np.sum(tau[:,j,:] * psmpsg),range(n)))

def PLo(tau,model_pdf):
    Q,n,n = tau.shape
    
    model_pdf.precompute()
    ret = 0
    for q in range(Q):
        for l in range(Q):
            ret += np.sum( tau[q,:,:] * tau[l,:,:].T * model_pdf.mplf[q,l,:,:] )

    return ret

def dH1_dgamma(gamma):
    Q,n = gamma.shape
    return -(gamma -1)*psi1(gamma) + ((np.sum(gamma,axis=0)-Q)*psi1(np.sum(gamma,axis=0))).reshape((1,n)).repeat(Q,axis=0)

def dH2_dtau(tau):
    return -np.log(tau)-1

def dPL1_dalpha(alpha,gamma):
    Q,n = gamma.shape
    return -n*(psi(alpha)-psi(np.sum(alpha))) + np.sum(psi_gamma_minus_psi_sgamma(gamma),axis=1)

def dPL1_dgamma(alpha,gamma):
    Q,n = gamma.shape
    return (alpha-1).reshape((Q,1)).repeat(n,axis=1)*psi1(gamma) - (np.sum(alpha)-Q)*psi1(np.sum(gamma,axis=0)).reshape((1,n)).repeat(Q,axis=0)

def dPL2_dgamma(gamma,tau):
    Q,n = gamma.shape
    return np.sum(tau,axis=1)*psi1(gamma)-n*psi1(np.sum(gamma,axis=0)).reshape((1,n)).repeat(Q,axis=0)

def dPL2_dtau(gamma,tau):
    Q,n = gamma.shape
    return psi_gamma_minus_psi_sgamma(gamma).reshape((Q,1,n)).repeat(n,axis=1)

def dPLo_dtau(tau,model_pdf):
    Q,n,n = tau.shape
    d = np.zeros((Q,n,n))

    model_pdf.precompute()

    for q in range(Q):
        for l in range(Q):
            d[q,:,:]+=tau[l,:,:].T * (model_pdf.mplf[q,l,:,:] + model_pdf.mplf[l,q,:,:].T)

    return d

def dPLo_dmodel(tau,model_pdf):
    Q,n,n = tau.shape
    d = np.zeros(model_pdf.n_params)
    for q in range(Q):
        for l in range(Q):
            dtheta = model_pdf.dlf(q,l)
            masque = tau[q,:,:] * tau[l,:,:].T
            for k in range(model_pdf.n_params):
                d[k]+=np.sum(masque*dtheta[k])
    return d

def phi_from_tau(tau):
    Q,n,n = tau.shape

    phi = np.zeros((Q-1,n,n))
    prodsin = np.ones((n,n))
    lprodsin = np.zeros((n,n))

    for q in range(Q-1):
        thelogcos = .5*np.log(tau[q,:,:])-lprodsin
        thelogcos[thelogcos>0]=0
        phi[q,:,:] = np.arccos( np.exp(thelogcos) )
        if q<Q-2:
            lprodsin += np.log(np.sin(phi[q,:,:]))
    return phi

def tau_from_phi(phi):
    Qm1,n,n = phi.shape
    Q = Qm1+1

    ltau = np.zeros((Q,n,n))
    for q in range(1,Q):
        ltau[q,:,:] = ltau[q-1,:,:] + np.log(np.sin(phi[q-1,:,:]))

    for q in range(Q-1):
        ltau[q,:,:] += np.log(np.cos(phi[q,:,:]))

    ltau *= 2
    return np.exp(ltau)

def df_dphi_from_df_dtau(df_dtau,phi):

    Q,n,n = df_dtau.shape

    df_dphi = np.zeros((Q-1,n,n))
    tau = tau_from_phi(phi)

    tan_phi = np.tan(phi)

    for q in range(Q):
        for l in range(Q-1):
            if l<=q:
                if l<q:
                    d = 2*tau[q,:,:]/tan_phi[l,:,:]
                if l==q:
                    d = -2*tau[q,:,:]*tan_phi[l,:,:]
                df_dphi[l,:,:] += df_dtau[q,:,:] * d
    return df_dphi

def J(gamma,tau,alpha,func):
    return H1(gamma) + H2(tau) + PL1(alpha,gamma) + PL2(gamma,tau) + PLo(tau,func)

def dJ_dgamma(gamma,tau,alpha,func):
    return dH1_dgamma(gamma) + dPL1_dgamma(alpha,gamma) + dPL2_dgamma(gamma,tau)

def dJ_dtau(gamma,tau,alpha,func):
    return dH2_dtau(tau) + dPL2_dtau(gamma,tau) + dPLo_dtau(tau,func)

def dJ_dalpha(gamma,tau,alpha,func):
    return dPL1_dalpha(alpha,gamma)

def dJ_dmodel(gamma,tau,alpha,func):
    return dPLo_dmodel(tau,func)


# Fun M_STEP

def m_step_vec_to_params(vec,Q,ModelClass,X,Y):
    alpha = vec[0:Q]
    func = ModelClass(Q,X,Y)
    func.set_params(vec[Q:])
    return (alpha,func)

def m_step_params_to_vec(alpha,func):
    return np.concatenate((alpha,func.get_params()))

def m_step_obj_fun(vec,Q,ModelClass,X,Y,gamma,tau):
    alpha,func = m_step_vec_to_params(vec,Q,ModelClass,X,Y)
    value=-J(gamma,tau,alpha,func) # MINUS BECAUSE MIN
    gradient=-np.concatenate((dJ_dalpha(gamma,tau,alpha,func),dJ_dmodel(gamma,tau,alpha,func))) # MINUS BECAUSE MIN
    return (value,gradient)
    
def m_step_constraints(Q,func):
    consts=[(1e-6,None)]*Q
    consts.extend(func.get_constraints())
    return consts

# Fun E_STEP

def e_step_vec_to_params(vec,Q,n):
    gamma = vec[0:(n*Q)].reshape((Q,n))
    phi = vec[(n*Q):].reshape((Q-1,n,n))
    tau = tau_from_phi(phi)
    return (gamma,tau,phi)

def e_step_params_to_vec(gamma,tau):
    Q,n = gamma.shape
    return np.concatenate((gamma.reshape(n*Q),phi_from_tau(tau).reshape((Q-1)*n*n)))

def e_step_obj_fun(vec,Q,n,alpha,func):
    gamma,tau,phi = e_step_vec_to_params(vec,Q,n)
    value=-J(gamma,tau,alpha,func) # MINUS BECAUSE MIN
    v1=dJ_dgamma(gamma,tau,alpha,func).reshape(n*Q)
    v2=df_dphi_from_df_dtau(dJ_dtau(gamma,tau,alpha,func),phi).reshape((Q-1)*n*n)
    gradient=-np.concatenate((v1,v2)) # MINUS BECAUSE MIN
    return (value,gradient)

def e_step_constraints(Q,n):
    consts = [(1e-4,None)] * (n*Q)
    consts.extend( [ (1e-2, .5*np.pi-1e-2) ] * ((Q-1)*n*n) )
    return consts

class Model_CLGM:

    def __init__(self,Model_PDF,Q,X,Y):

        self.n = X.shape[0]
        self.Q = Q

        # net
        self.X = X
        self.Y = Y
        
        self.Model_PDF = Model_PDF

        #params
        self.alpha = np.ones(Q)
        self.func = Model_PDF(Q,X,Y)

        #variationnal
        
        # layer 1
        self.gamma = np.ones((self.Q,self.n))

        # layer 2
        Xt = self.func.transf()
        L1=np.sqrt(np.diag(1/np.sum(Xt,axis=1)))*np.matrix(Xt)*np.sqrt(np.diag(1/np.sum(Xt,axis=0)))
        L=L1*L1.T
        E=sp.linalg.eigh(L)
        data=E[1][:,np.abs(E[0]).argsort()[-Q:]]/np.sqrt(np.sort(np.abs(E[0]))[-Q:]).reshape((1,Q)).repeat(self.n,axis=0)
        while True:
            try:
                c=spcv.kmeans2(data,Q,missing='raise')
            except spcv.ClusterError:
                pass
            else:
                break
        self.tau = np.zeros((Q,self.n,self.n))
        for i in range(self.n):
            self.tau[c[1][i],:,i]=1.

        self.tau+=.1
        self.tau /= np.repeat(np.sum(self.tau,axis=0).reshape((1,self.n,self.n)),self.Q,axis=0)

    def m_step(self):
        r=spo.fmin_l_bfgs_b(m_step_obj_fun,m_step_params_to_vec(self.alpha,self.func),args=(self.Q,self.Model_PDF,self.X,self.Y,self.gamma,self.tau),bounds=m_step_constraints(self.Q,self.func),m=10,pgtol=1e-6)
        self.alpha,self.func = m_step_vec_to_params(r[0],self.Q,self.Model_PDF,self.X,self.Y)
        print " "*13+"M step: %5u iter, %5u fun evals" % (r[2]['nit'], r[2]['funcalls'])

        return None

    def e_step(self):
        r=spo.fmin_l_bfgs_b(e_step_obj_fun,e_step_params_to_vec(self.gamma,self.tau),args=(self.Q,self.n,self.alpha,self.func),bounds=e_step_constraints(self.Q,self.n),m=10,pgtol=1e-6)
        self.gamma,self.tau,phi = e_step_vec_to_params(r[0],self.Q,self.n)
        print " "*13+"E step: %5u iter, %5u fun evals" % (r[2]['nit'], r[2]['funcalls'])
        return r

    def J(self):
        return J(self.gamma,self.tau,self.alpha,self.func)

    def em(self):
        oldJ = -np.inf
        it=0
        self.m_step()

        while(True):
            newJ = self.J()
            print "iter = %5u\t J = %f" % (it,newJ)
            if(newJ-oldJ<1e-4):
                break
            oldJ=newJ
            it+=1
            self.e_step()
            self.m_step()

        return it




    



        






